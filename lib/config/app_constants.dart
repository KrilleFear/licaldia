abstract class AppConstants {
  static const String appName = 'Licaldia';
  static const String sourceCodeUrl = 'https://gitlab.com/KrilleFear/licaldia';
  static const String helpUrl =
      'https://gitlab.com/KrilleFear/licaldia/-/issues';
}
