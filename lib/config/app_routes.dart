import 'package:licaldia/pages/diary.dart';
import 'package:licaldia/pages/home.dart';
import 'package:licaldia/pages/intro.dart';
import 'package:licaldia/pages/profile.dart';
import 'package:licaldia/pages/settings.dart';
import 'package:flutter/material.dart';
import 'package:vrouter/vrouter.dart';

abstract class AppRoutes {
  static List<VRouteElement> routes = [
    VWidget(
      path: '/',
      widget: const Home(),
      buildTransition: _fadeTransition,
    ),
    VWidget(
      path: '/intro',
      widget: const Intro(),
      buildTransition: _fadeTransition,
    ),
    VWidget(
      path: '/diary',
      widget: const Diary(),
      buildTransition: _fadeTransition,
      stackedRoutes: [
        VWidget(
          path: '/profile',
          widget: const Profile(),
          buildTransition: _fadeTransition,
        ),
        VWidget(
          path: '/settings',
          widget: const Settings(),
          buildTransition: _fadeTransition,
        ),
      ],
    ),
  ];

  static Widget _fadeTransition(
    Animation<double> animation1,
    Animation<double> animation2,
    Widget child,
  ) =>
      FadeTransition(
        opacity: animation1,
        child: child,
      );
}
