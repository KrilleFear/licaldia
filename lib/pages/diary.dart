import 'package:licaldia/model/diary_entry.dart';
import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/utils/show_number_picker_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import 'views/diary_view.dart';

enum DiaryEntryAction { repeat, delete, cantDelete }

class Diary extends StatefulWidget {
  const Diary({Key? key}) : super(key: key);

  @override
  DiaryController createState() => DiaryController();
}

class DiaryController extends State<Diary> {
  final TextEditingController descriptionTextField = TextEditingController();
  String get balanceText =>
      '${L10n.of(context)!.yourBalance}: ${(Licaldia.of(context).balance).round()}Kcal';

  double calories = 0;

  void setCalories(double newCalories) =>
      setState(() => calories = newCalories);

  List<DiaryEntry> get diaryEntries => Licaldia.of(context)
      .diaryEntries
      .where((entry) => entry.calories != 0)
      .toList()
    ..sort((a, b) => b.dateTime.compareTo(a.dateTime));

  final double min = -1000;
  final double max = 1000;

  void tapFoodAction() async {
    final cal = await showNumberPickerDialog(
      title: L10n.of(context)!.newFoodEntry,
      context: context,
      minValue: 0,
      maxValue: max.round(),
      value: calories < 0 ? 0 : calories.round(),
      measurement: 'Kcal',
    );
    if (cal != null) setState(() => calories = cal.toDouble());
  }

  void tapSportsAction() async {
    final cal = await showNumberPickerDialog(
      title: L10n.of(context)!.newWorkoutSession,
      context: context,
      minValue: 0,
      maxValue: min.round() * (-1),
      value: calories > 0 ? 0 : calories.round() * (-1),
      measurement: 'Kcal',
    );
    if (cal != null) setState(() => calories = cal.toDouble() * (-1));
  }

  void diaryEntryAction(DiaryEntryAction action, DiaryEntry entry) {
    switch (action) {
      case DiaryEntryAction.repeat:
        Licaldia.of(context).addDiaryEntry(entry.title, entry.calories);
        break;
      case DiaryEntryAction.delete:
        if (Licaldia.of(context).diaryEntries.last.calories == 0) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text(L10n.of(context)!.thisCanNotBeDeleted)),
          );
          return;
        }
        Licaldia.of(context).deleteLatestDiaryEntry();
        break;
      case DiaryEntryAction.cantDelete:
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(L10n.of(context)!.youCanOnlyDeleteNewest)),
        );
        break;
    }
  }

  Color get caloriesColor => calories < 0
      ? Colors.blue
      : calories == 0
          ? Colors.green
          : Colors.orange;

  String get statusText => Licaldia.of(context).balance > 10000
      ? L10n.of(context)!.congratulationsYouAreDead
      : Licaldia.of(context).balance > 2000
          ? L10n.of(context)!.youShouldDefinetlyEatSomething
          : Licaldia.of(context).balance > 1000
              ? L10n.of(context)!.maybeYouShouldEatSomething
              : Licaldia.of(context).balance < -500
                  ? L10n.of(context)!.youEatTooMuch
                  : Licaldia.of(context).balance < -1000
                      ? L10n.of(context)!.youEatWayTooMuch
                      : L10n.of(context)!.youArePerfectlyInBalnce;

  Color get statusColor => Licaldia.of(context).balance > 10000
      ? Colors.black
      : Licaldia.of(context).balance > 2000
          ? Colors.red
          : Licaldia.of(context).balance > 1000
              ? Colors.orange
              : Licaldia.of(context).balance < -500
                  ? Colors.orange
                  : Licaldia.of(context).balance < -1000
                      ? Colors.red
                      : Colors.green;

  void saveAction() {
    final title = descriptionTextField.text.isEmpty
        ? calories > 0
            ? L10n.of(context)!.newFoodEntry
            : L10n.of(context)!.newWorkoutSession
        : descriptionTextField.text;
    Licaldia.of(context).addDiaryEntry(title, calories);
    descriptionTextField.clear();
    setState(() => calories = 0);
  }

  String get caloriesConsumedLocalized =>
      '${calories > 0 ? L10n.of(context)!.caloriesConsumed : L10n.of(context)!.caloriesBurned}: ${calories.round().abs()}Kcal';
  @override
  Widget build(BuildContext context) => DiaryView(this);
}

extension SameDate on DateTime {
  bool sameDate(DateTime other) {
    return day == other.day && month == other.month && year == other.year;
  }

  String toLocalizedTimeOfDay(BuildContext context) =>
      L10n.of(context)!.timeOfDay(
        hour > 12 ? hour - 12 : hour,
        hour,
        minute,
        hour > 12 ? 'pm' : 'am',
      );

  String toLocalizedDate(BuildContext context) {
    final sameYear = DateTime.now().year == year;
    if (sameDate(DateTime.now())) {
      return L10n.of(context)!.today;
    }
    if (sameYear) {
      return L10n.of(context)!.dateWithoutYear(
          month.toString().padLeft(2, '0'), day.toString().padLeft(2, '0'));
    }
    return L10n.of(context)!.dateWithYear(year.toString(),
        month.toString().padLeft(2, '0'), day.toString().padLeft(2, '0'));
  }
}
