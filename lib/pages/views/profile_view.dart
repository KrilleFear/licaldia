import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/widgets/app_bottom_navigation_bar.dart';
import 'package:licaldia/widgets/title_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../../utils/localized_strings.dart';
import '../profile.dart';

class ProfileView extends StatelessWidget {
  final ProfileController controller;

  const ProfileView(this.controller, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: Licaldia.of(context).onUpdate.stream,
        builder: (context, snapshot) {
          return Scaffold(
            appBar: AppBar(
              automaticallyImplyLeading: false,
              title: Text(L10n.of(context)!.profile),
            ),
            body: ListView(
              children: [
                TitleListTile(label: L10n.of(context)!.bodyMeasurementIndex),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      width: 256,
                      height: 256,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(9999),
                        color: controller.bmiColor,
                      ),
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Text(
                            controller.bmi,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 80,
                            ),
                          ),
                          Text(
                            controller.bmiStatus,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                TitleListTile(label: L10n.of(context)!.goal),
                ListTile(
                  title: Text(
                    '${L10n.of(context)!.dailyCalories}: ${controller.dailyGoal.round()}Kcal',
                  ),
                  subtitle: Text(
                    controller.dailyGoalLocalized,
                    style: TextStyle(color: controller.dailyGoalColor),
                  ),
                  trailing: IconButton(
                    icon: const Icon(Icons.refresh),
                    onPressed: controller.resetDailyGoal,
                  ),
                ),
                Slider(
                  min: controller.minDailyGoal,
                  max: controller.maxDailyGoal,
                  divisions: 100,
                  value: controller.dailyGoal,
                  onChanged: controller.changeDailyGoal,
                  onChangeEnd: controller.setDailyGoal,
                  activeColor: controller.dailyGoalColor,
                  label: controller.dailyBalanceLocalized(),
                  semanticFormatterCallback: controller.dailyBalanceLocalized,
                ),
                TitleListTile(label: L10n.of(context)!.profile),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          child: DropdownButton<Sex>(
                            isExpanded: true,
                            value: controller.sexValue,
                            onChanged: controller.setSex,
                            underline: Container(),
                            items: Sex.values
                                .map((sex) => DropdownMenuItem(
                                      value: sex,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          sex.toLocalizedString(context),
                                        ),
                                      ),
                                    ))
                                .toList(),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          child: DropdownButton<int>(
                            isExpanded: true,
                            value: controller.ageValue,
                            onChanged: controller.setAge,
                            underline: Container(),
                            items: [
                              for (var i = 12; i <= 100; i++) i,
                            ]
                                .map((age) => DropdownMenuItem(
                                      value: age,
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                            '${L10n.of(context)!.age}: $age'),
                                      ),
                                    ))
                                .toList(),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: Card(
                    child: DropdownButton<DailyActivity>(
                      isExpanded: true,
                      value: controller.dailyActivityValue,
                      onChanged: controller.setDailyActivity,
                      underline: Container(),
                      items: DailyActivity.values
                          .map((dailyActivity) => DropdownMenuItem(
                                value: dailyActivity,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                      '${L10n.of(context)!.dailyActivity}: ${controller.dailyActivityLocalized(dailyActivity)}'),
                                ),
                              ))
                          .toList(),
                    ),
                  ),
                ),
                TitleListTile(label: L10n.of(context)!.bodyMeasurements),
                Row(
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 2,
                            backgroundColor: Theme.of(context).cardColor,
                            foregroundColor:
                                Theme.of(context).textTheme.bodyText1?.color,
                          ),
                          onPressed: controller.setHeight,
                          child: Text(controller.currentHeight),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            elevation: 2,
                            backgroundColor: Theme.of(context).cardColor,
                            foregroundColor:
                                Theme.of(context).textTheme.bodyText1?.color,
                          ),
                          onPressed: controller.setWeight,
                          child: Text(controller.currentWeight),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 1),
          );
        });
  }
}
