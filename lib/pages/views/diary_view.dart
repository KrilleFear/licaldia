import 'package:licaldia/model/licaldia.dart';
import 'package:licaldia/widgets/app_bottom_navigation_bar.dart';
import 'package:licaldia/widgets/balance_display.dart';
import 'package:licaldia/widgets/title_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

import '../diary.dart';

class DiaryView extends StatelessWidget {
  final DiaryController controller;

  const DiaryView(this.controller, {Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Object>(
        stream: Licaldia.of(context).onUpdate.stream,
        builder: (context, snapshot) {
          return Scaffold(
            appBar: AppBar(
              centerTitle: true,
              title: const BalanceDisplay(),
            ),
            body: ListView.builder(
              itemCount: controller.diaryEntries.length + 1,
              itemBuilder: (context, i) {
                if (i == 0) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding:
                            const EdgeInsets.only(left: 12, top: 12, right: 12),
                        child: Card(
                          color: controller.statusColor,
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Text(controller.statusText,
                                  style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(12),
                        child: Card(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ListTile(
                                title: Center(
                                  child: Text(
                                    controller.calories == 0
                                        ? L10n.of(context)!.newDiaryEntry
                                        : controller.calories < 0
                                            ? L10n.of(context)!
                                                .newWorkoutSession
                                            : L10n.of(context)!.newFoodEntry,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                subtitle: Center(
                                  child: Text(
                                    controller.caloriesConsumedLocalized,
                                    style: TextStyle(
                                      color: controller.caloriesColor,
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                children: [
                                  IconButton(
                                    splashRadius: 24,
                                    icon: const Icon(Icons.run_circle_outlined),
                                    onPressed: controller.tapSportsAction,
                                  ),
                                  Expanded(
                                    child: Slider(
                                      divisions: 200,
                                      min: controller.min,
                                      max: controller.max,
                                      value: controller.calories,
                                      onChanged: controller.setCalories,
                                      label:
                                          controller.caloriesConsumedLocalized,
                                      activeColor: controller.caloriesColor,
                                    ),
                                  ),
                                  IconButton(
                                    splashRadius: 24,
                                    icon: const Icon(Icons.fastfood_outlined),
                                    onPressed: controller.tapFoodAction,
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: TextField(
                                  controller: controller.descriptionTextField,
                                  decoration: InputDecoration(
                                    labelText: L10n.of(context)!.description,
                                    hintText:
                                        L10n.of(context)!.foodOrWorkoutSession,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: SizedBox(
                                  width: double.infinity,
                                  child: ElevatedButton.icon(
                                    icon: const Icon(Icons.save),
                                    onPressed: controller.calories == 0
                                        ? null
                                        : controller.saveAction,
                                    label: Text(L10n.of(context)!.save),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  );
                }
                i--;
                final entry = controller.diaryEntries[i];
                final showDateTime = i == 0 ||
                    (i > 0 &&
                        !entry.dateTime
                            .sameDate(controller.diaryEntries[i - 1].dateTime));
                return Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    if (showDateTime)
                      TitleListTile(
                          label: entry.dateTime.toLocalizedDate(context)),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        child: PopupMenuButton<DiaryEntryAction>(
                          onSelected: (action) =>
                              controller.diaryEntryAction(action, entry),
                          itemBuilder: (_) => [
                            PopupMenuItem(
                                value: DiaryEntryAction.repeat,
                                child: Text(L10n.of(context)!.repeat)),
                            if (i == 0)
                              PopupMenuItem(
                                  value: DiaryEntryAction.delete,
                                  child: Text(
                                    L10n.of(context)!.delete,
                                    style: const TextStyle(color: Colors.red),
                                  )),
                            if (i != 0)
                              PopupMenuItem(
                                  value: DiaryEntryAction.cantDelete,
                                  child: Text(
                                    L10n.of(context)!.delete,
                                    style: const TextStyle(
                                      color: Colors.grey,
                                      decoration: TextDecoration.lineThrough,
                                    ),
                                  )),
                          ],
                          child: ListTile(
                            leading: CircleAvatar(
                              backgroundColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                              child: Icon(
                                entry.calories > 0
                                    ? Icons.fastfood_outlined
                                    : Icons.run_circle_outlined,
                                color: entry.calories > 0
                                    ? Colors.green
                                    : Colors.blue,
                              ),
                            ),
                            title: Row(
                              children: [
                                Text(entry.title),
                                const Spacer(),
                                Text(entry.dateTime
                                    .toLocalizedTimeOfDay(context))
                              ],
                            ),
                            subtitle: Text(
                              '${entry.calories > 0 ? L10n.of(context)!.caloriesConsumed : L10n.of(context)!.caloriesBurned}: ${entry.calories.round().abs()}Kcal',
                              style:
                                  const TextStyle(fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
            bottomNavigationBar: const AppBottomNavigationBar(currentIndex: 0),
          );
        });
  }
}
