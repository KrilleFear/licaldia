import 'dart:developer';
import 'dart:ui';

import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:licaldia/config/app_constants.dart';
import 'package:licaldia/model/licaldia.dart';
import 'package:timezone/data/latest_all.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:flutter_gen/gen_l10n/l10n.dart';

extension NotificationExtension on Licaldia {
  Future<void> updateSheduledNotifications() async {
    final l10n = await L10n.delegate.load(window.locale);
    log('Update sheduled notifications...');
    tz.initializeTimeZones();
    final currentTimeZone = await FlutterNativeTimezone.getLocalTimezone();
    final location = tz.getLocation(currentTimeZone);
    tz.setLocalLocation(location);

    final flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    final notificationDetails = NotificationDetails(
      android: AndroidNotificationDetails(
        AppConstants.appName,
        AppConstants.appName,
        channelDescription: l10n.notifications,
      ),
      iOS: const IOSNotificationDetails(
        presentAlert: true,
        presentBadge: true,
        presentSound: true,
      ),
    );

    final tomorrow = DateTime.now(); //.add(const Duration(days: 1));

    if (breakfastNotification) {
      await flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        l10n.breakfastReminder,
        l10n.dontForgetToUpdateYourCalorieDiary,
        tz.TZDateTime.from(
            DateTime(
              tomorrow.year,
              tomorrow.month,
              tomorrow.day,
              breakfastNotificationTime.hour,
              breakfastNotificationTime.minute,
            ),
            location),
        notificationDetails,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
      );
      log('Updated breakfast notifications!');
    } else {
      flutterLocalNotificationsPlugin.cancel(0);
    }

    if (lunchNotification) {
      await flutterLocalNotificationsPlugin.zonedSchedule(
        1,
        l10n.lunchReminder,
        l10n.dontForgetToUpdateYourCalorieDiary,
        tz.TZDateTime.from(
            DateTime(
              tomorrow.year,
              tomorrow.month,
              tomorrow.day,
              lunchNotificationTime.hour,
              lunchNotificationTime.minute,
            ),
            location),
        notificationDetails,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
      );
      log('Updated lunch notifications!');
    } else {
      flutterLocalNotificationsPlugin.cancel(1);
    }

    if (dinnerNotification) {
      await flutterLocalNotificationsPlugin.zonedSchedule(
        2,
        l10n.dinnerReminder,
        l10n.dontForgetToUpdateYourCalorieDiary,
        tz.TZDateTime.from(
            DateTime(
              tomorrow.year,
              tomorrow.month,
              tomorrow.day,
              dinnerNotificationTime.hour,
              dinnerNotificationTime.minute,
            ),
            location),
        notificationDetails,
        androidAllowWhileIdle: true,
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
      );
      log('Updated dinner notifications!');
    } else {
      flutterLocalNotificationsPlugin.cancel(2);
    }
    log('Sheduled notifications updated!');
  }
}
