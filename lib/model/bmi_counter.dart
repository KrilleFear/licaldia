// Copyright (C) 2020 Christian Pauly
//
// This file is part of open_calories_tracker.
//
// open_calories_tracker is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// open_calories_tracker is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with open_calories_tracker.  If not, see <http://www.gnu.org/licenses/>.

enum BmiCategory {
  severeUnderweightGrade2,
  severeUnderweightGrade1,
  moderatelyUnderweight,
  slightlyUnderweight,
  normalWeight,
  preObesity,
  grade1Obesity,
  grade2Obesity,
  grade3Obesity,
}

class BmiCounter {
  num heightInCm;
  num weightInKilogram;
  num get _heightInMeter => heightInCm / 100;

  BmiCounter(this.heightInCm, this.weightInKilogram);

  num get bmi => weightInKilogram / (_heightInMeter * _heightInMeter);

  num get roundedBmi => (bmi * 10).round() / 10;

  BmiCategory get category {
    final bmi = this.bmi;
    if (bmi < 13) return BmiCategory.severeUnderweightGrade2;
    if (bmi < 16) return BmiCategory.severeUnderweightGrade1;
    if (bmi < 17) return BmiCategory.moderatelyUnderweight;
    if (bmi < 18.5) return BmiCategory.slightlyUnderweight;
    if (bmi < 25) return BmiCategory.normalWeight;
    if (bmi < 30) return BmiCategory.preObesity;
    if (bmi < 35) return BmiCategory.grade1Obesity;
    if (bmi < 40) return BmiCategory.grade2Obesity;
    return BmiCategory.grade3Obesity;
  }
}
